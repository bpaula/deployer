# Run this playbook to deploy the entire Agave stack, including auth, core and db. This playbook should not be used
# to update an existing deployment as initial database loads will be redone resulting in data loss.
#
# NOTE: This playbook requires Ansible v2.0+
#
# The parameters for this playbook are:
# 1. db_remote_user, core_remote_user, auth_remote_user - the linux user account used for connecting via ssh.
# 2. tenant_id: determines which tenant to deploy; an existing directory within the tenants directory must already exist.
# 3. load_auth_sql_data - (true/false). This should be set to true the first time and false after since, attemping to
#    reload the sql data
# 3. core_version (OPTIONAL): version (branch) to deploy; default is 2.1.4
#
# Example invocation:
# $ ansible-playbook -i host_files/ec2_hosts deploy_agave.plbk -e tenant_id=dev_sandbox -e core_config_file=dev_sandbox -e deploy_core_default_templates=True
# deployer -i /deploy/host_files/slt_sandbox_hosts /deploy/deploy_agave.plbk -e tenant_id=dev_sandbox -e core_config_file=dev_sandbox -e docker_version=1.12.6-1.el7.centos -e docker_compose_version=1.11.1 -e deploy_core_default_templates=True

---

# first, deploy databases
- hosts: db
  roles:
      - agave_host
      - docker_host
      - agave_db_onehost
  vars:
      - update_docker_version: True
      - update_docker_compose_version: True
      - clean_host: True

# load auth data
- hosts: db
  roles:
      - mysql_apim
  vars:
      - env: sandbox
      - mysql_root_user: root
      - mysql_root_password: password
      - create_tenant_mysql_user: true

# deploy core services and run migrations
- hosts: core
  roles:
      - agave_host
      - docker_host
      - { role: agave_core_compose_repo, when: deploy_core_default_templates == false }
      - agave_core
      - agave_core_sql_migrations
  vars:
      - remove_apim_dir: True
      - update_docker_version: True
      - update_docker_compose_version: True
      - core_migrations_command: migrate

# deploy the auth services
- hosts: auth
  vars:
      - env: sandbox
      - mysql_root_user: root
      - mysql_root_password: password
      - clean_host: True
  roles:
      - agave_host
      - docker_host
      - agave_auth
      - auth_rolling_deploy
      - ldap_apim
  vars:
      - clean_host: True
      - update_docker_version: True
      - update_docker_compose_version: True
      - remove_all_auth_containers: True
      - deploy_core_api_templates: True

# add log rotation and boutique apis
- hosts: auth
  roles:
    - logrotate
    - boutique_apis

######
# test runner
# hosts: test_runner
# roles:
# vars:

######