<%@ page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>
    <%
        loginFailed = CharacterEncoder.getSafeText(request.getParameter("loginFailed"));
        if (loginFailed != null) {
    %>
        <div class="card login-error">
          <div class="card-content center-align">
                <fmt:message key='<%=CharacterEncoder.getSafeText(request.getParameter("errorMessage"))%>'/>
          </div>
        </div>
    <% } %>

    <% if (CharacterEncoder.getSafeText(request.getParameter("username")) == null || "".equals(CharacterEncoder.getSafeText(request.getParameter("username")).trim())) { %>

        <!-- Username -->
        <input type="text" id="username" name="username" label="Username" placeholder="Username" class="validate" size="30" required/>

    <%} else { %>
        <input type="hidden" id="username" name="username" value='<%=CharacterEncoder.getSafeText(request.getParameter("username"))%>'/>
    <% } %>

        <!--Password-->
        <input type="password" id="password" name="password" placeholder="Password" class="validate" size='30'/>
        <input type="hidden" name="sessionDataKey" value='<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>'/>
        <button type="submit" id="login-btn" value="Log In" class="btn center-align waves-effect waves-light">Log In</button>

        <div class="help-block text-center">
          <span class="forgot-password">
            <a href="https://sgci.agaveapi.co/reset_password" target="_blank">Forgot Password</a>
          </span> | <a href="http://status.agaveapi.co" target="_blank" title="Platform Status">Platform Status
        </div>
