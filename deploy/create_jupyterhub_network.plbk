---
################################################################################
#
#   CREATE_JUPYTERHUB_NETWORK.YML
#   Kulisics, Joseph D.
#   2016 June 30
#
#   REQUIRED VARIABLES:
#
################################################################################

- name: Setup Major Variables in Local Host's Facts
  hosts: local

  vars_prompt:

    - name: jupyterhub_namespace
      prompt: "Enter Jupyterhun Namespacing Prefix"
      private: no
      default: jupyter2
    - name: jupyterhub_security_group_namespace
      prompt: "Enter Jupyterhun Security Group Namespacing Prefix"
      private: no
      default: jupyter2

    - name: os_authurl
      prompt: "Enter OpenStack Identity URL"
      private: no
      default: https://tacc.jetstream-cloud.org:5000/v3
#      default: https://jblb.jetstream-cloud.org:5000/v3
    - name: os_domain
      prompt: "Enter OpenStack Domain"
      private: no
      default: tacc
    - name: os_project
      prompt: "Enter OpenStack Project"
      private: no
      default: TG-ASC160018
    - name: os_username
      prompt: "Enter OpenStack Username"
      private: no
      default: apitest
    - name: os_password
      prompt: "Enter OpenStack Password"
      private: yes 

    - name: os_region
      prompt: "Enter OpenStack Region"
      private: no 
      default: RegionOne
    - name: os_master_flavor
      prompt: "Enter OpenStack Master Flavor"
      private: no 
      default: m1.medium
    - name: os_worker_flavor
      prompt: "Enter OpenStack Worker Flavor"
      private: no 
      default: m1.large
    - name: os_zone
      prompt: "Enter OpenStack Availability Zone"
      private: no 
      default: nova
    - name: os_image
      prompt: "Enter OpenStack Image ID"
      private: no 
      default: 9b8bfe7b-ec03-4448-9f7a-c8f5818c2431
#      default: 3de3b7ea-6d1d-43b5-9d43-e428f1b5dbc6
    - name: os_key_file_src
      prompt: "Enter Path to OpenStack Public Key File Source"
      private: no 
      default: ~/.ssh/apitest_postman.key.pub

    - name: number_of_workers
      prompt: "Enter the Number of Workers"
      private: no 
      default: 2


  tasks:

    - name: Set Facts Collected from Prompts

      set_fact:

        jupyterhub_namespace: "{{jupyterhub_namespace}}"
        jupyterhub_security_group_namespace: "{{jupyterhub_security_group_namespace}}"

        os_authurl: "{{os_authurl}}"
        os_domain: "{{os_domain}}"
        os_project: "{{os_project}}"
        os_username: "{{os_username}}"
        os_password: "{{os_password}}"

        os_region: "{{os_region}}"
        os_master_flavor: "{{os_master_flavor}}"
        os_worker_flavor: "{{os_worker_flavor}}"
        os_zone: "{{os_zone}}"
        os_image: "{{os_image}}"
        os_network: "{{jupyterhub_namespace}}_net"
        os_keyname: "{{jupyterhub_namespace}}-key"
        os_key_file: "/tmp/{{jupyterhub_namespace}}-key.pub"
        os_key_file_src: "{{os_key_file_src}}"

        number_of_workers: "{{number_of_workers}}"

- name: Put Key File on VM Manager
  hosts: deployer
  remote_user: rodeo
  become: yes
  become_user: root
  become_method: sudo

  vars:

    os_key_file: "{{hostvars[groups['local'][0]]['os_key_file']}}"
    os_key_file_src: "{{hostvars[groups['local'][0]]['os_key_file_src']}}"

  tasks:

    - name: Copy Key File to VM Creator

      copy:

        src: "{{os_key_file_src}}"
        dest: "{{os_key_file}}"


- name: Use Singleton Deployer Group System to Create VMs 
  hosts: deployer
  remote_user: rodeo
  become: yes
  become_user: root
  become_method: sudo

  vars:

    jupyterhub_namespace: "{{hostvars[groups['local'][0]]['jupyterhub_namespace']}}"
    jupyterhub_security_group_namespace: "{{hostvars[groups['local'][0]]['jupyterhub_security_group_namespace']}}"

    os_manager_vm_name: "{{jupyterhub_namespace}}-dev-manager"
    os_worker_vm_name: "{{jupyterhub_namespace}}-dev-worker"

    os_manager_security_group: "{{jupyterhub_security_group_namespace}}.manager"
    os_worker_security_group: "{{jupyterhub_security_group_namespace}}.worker"


    os_authurl: "{{hostvars[groups['local'][0]]['os_authurl']}}"
    os_domain: "{{hostvars[groups['local'][0]]['os_domain']}}"
    os_project: "{{hostvars[groups['local'][0]]['os_project']}}"
    os_username: "{{hostvars[groups['local'][0]]['os_username']}}"
    os_password: "{{hostvars[groups['local'][0]]['os_password']}}"

    os_region: "{{hostvars[groups['local'][0]]['os_region']}}"
    os_master_flavor: "{{hostvars[groups['local'][0]]['os_master_flavor']}}"
    os_worker_flavor: "{{hostvars[groups['local'][0]]['os_worker_flavor']}}"
    os_zone: "{{hostvars[groups['local'][0]]['os_zone']}}"
    os_image: "{{hostvars[groups['local'][0]]['os_image']}}"
    os_network: "{{hostvars[groups['local'][0]]['os_network']}}"
    os_keyname: "{{hostvars[groups['local'][0]]['os_keyname']}}"
    os_key_file: "{{hostvars[groups['local'][0]]['os_key_file']}}"

    number_of_workers: "{{hostvars[groups['local'][0]]['number_of_workers']}}"

  roles:

    - jupyterhub_vm_manager
    - jupyterhub_vm_creator
    - jupyterhub_vm_security_group_creator
    - jupyterhub_manager_vm_creator
    - jupyterhub_worker_vm_creator


- name: Set up Created Inventory and Check VM Readiness
  hosts: local


  roles:

    - jupyterhub_inventory_builder


  tasks:


    - name: Wait for SSH
      with_items: "{{openstack_servers}}"
      wait_for:
        host: "{{item.public_v4}}"
        port: 22
        state: started
        connect_timeout: 10
        delay: 15
        timeout: 300
